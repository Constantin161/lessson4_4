<?php

include('config.php');

$pdo = new PDO(DB, LOGIN, PW);

$sql = "SHOW TABLES";
$tables = getData($pdo, $sql);

$table_name = !empty($_POST['table']) ? $_POST['table'] : null;

if ($table_name) {
    $sql = "DESCRIBE {$table_name}";
    $data = getData($pdo, $sql);
}

include('form.html');

function getData($pdo, $sql) {
	$stm = $pdo->prepare($sql);
    $stm->execute();
    return $stm->fetchAll(PDO::FETCH_NUM);
}